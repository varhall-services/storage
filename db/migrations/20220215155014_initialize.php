<?php
declare(strict_types=1);

use Phinx\Migration\AbstractMigration;

final class Initialize extends AbstractMigration
{
    public function change(): void
    {
        $this->table('files', [ 'id' => false, 'primary_key' => 'id' ])
            ->addColumn('id', 'char', [ 'length' => 36 ])
            ->addColumn('name', 'string')
            ->addColumn('namespace', 'string')
            ->addTimestamps()
            //->addColumn('deleted_at', 'timestamp', [ 'null' => true ])
            ->create();

        $this->table('versions', [ 'id' => false, 'primary_key' => 'id' ])
            ->addColumn('id', 'char', [ 'length' => 36 ])
            ->addColumn('file_id', 'char', [ 'length' => 36 ])
            ->addColumn('version', 'string', [ 'length' => 100 ])
            ->addColumn('size', 'integer')
            ->addColumn('mime_type', 'string', [ 'length' => 50 ])
            ->addColumn('info', 'json')
            ->addForeignKey('file_id', 'files', 'id', ['delete'=> 'CASCADE', 'update'=> 'CASCADE'])
            ->addIndex('version')
            ->create();
    }
}
