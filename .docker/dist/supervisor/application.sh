#!/bin/bash

# Initialize environment variables
export $(grep -v ' ' /etc/environment | xargs -d '\n')

# Run application if environment variable is set
if [[ $RABBIT_ENABLED == "1" ]]
then
  php /var/www/html/bin/console rabbitmq:consumer storage 3600
fi