#!/bin/sh

# WARNING, line ending must be Lf

php /usr/local/startup/startup.php | bash

# Execute arguments
# exec "$@"



# Original entrypoint
# https://github.com/docker-library/php/blob/master/8.0/buster/apache/docker-php-entrypoint
set -e

# first arg is `-f` or `--some-option`
if [ "${1#-}" != "$1" ]; then
	set -- apache2-foreground "$@"
fi

# Execute arguments
exec "$@"
