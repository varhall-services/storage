# Files Service

- [Installation](#installation)
- [Environment variables](#environment-variables)
- [Configuration](#configuration)
- [Usage](#usage)

Files Service is universal microservice used for image manipulation build with [Nette Framework](https://nette.org/en).

<a name="installation"></a>
## Installation

### Docker

Files service is designed to run in Docker. Below is example of docker-compose configuration. More information about Docker images can be found in [official documenatation](https://docs.docker.com/compose/compose-file/compose-file-v3/)

    version: '3'
    services:
        app:
            image: registry.gitlab.com/varhall-services/storage/storage:latest
            ports: 
                - 8080:80
            volumes:
            environment:
                DATABASE_HOST: database
                DATABASE_USER: root
                DATABASE_PASSWORD: 123456
                DATABASE_NAME: app
                RABBIT_HOST: rabbit
                RABBIT_USER: guest
                RABBIT_PASSWORD: guest
                RABBIT_PORT: 5672
                APP_ENV: development

        database:
            image: mariadb:latest
            environment:
                MYSQL_ROOT_PASSWORD: '123456'
                MYSQL_DATABASE: 'app'
            command: mysqld --character-set-server=utf8 --collation-server=utf8_czech_ci


Since the service is based on standard Apache PHP Docker image, the application is located in `/var/www/html/` directory.

### Storage

Application stores the files in `www/files` subdirectory. So use this directory as volume for the files to be persistent.

<a name="environment-variables"></a>
## Environment variables

| Variable          | Required | Description                                                      |
|-------------------|----------|------------------------------------------------------------------|
| DATABASE_HOST     | Yes      | MySQL Database server name                                       |
| DATABASE_USER     | Yes      | Database user                                                    |
| DATABASE_PASSWORD | Yes      | Database password                                                |
| DATABASE_NAME     | Yes      | Database name                                                    |
| RABBIT_HOST       | No       | RabbitMQ host, default rabbitmq                                  |
| RABBIT_USER       | No       | RabbitMQ user, default guest                                     |
| RABBIT_PASSWORD   | No       | RabbitMQ password, default guest                                 |
| APP_ENV           | No       | Allows to switch service to debug mode using value 'development' |

<a name="configuration"></a>
## Configuration

It is possible to configure image resizing to provide various versions (eg. large, medium and preview).
The easiest way to define the rules, override `app/config/local.neon` file using `COPY` command in Dockerfile build phase
or mount it as volume.

The content of configuration file could be:

    parameters:
        configurations:
            default:
                handler: @handlers.file
    
            articles:
                handler: @handlers.image
                versions:
                    original:
                    large: '1920x600:SHRINK_ONLY'
                    medium: '800x280:SHRINK_ONLY|STRETCH'
                    preview: '300x300:FILL'

            shop:
                handler: @handlers.image
                versions:
                    large: '1920x900:SHRINK_ONLY'
                    small: '300x150:SHRINK_ONLY'


Each configuration section represents namespace where the images are stored. Each namespace has
a file handler which can handle some file version transformations. If the configuration is not defined
for given namespace or the namespace is invalid, `default` named section is used. In case there is no 
`default` section, the file is stored in single version without any transformations.

## Handlers

Handler is a file transformation class responsible for creating file versions. Typical file handler usage
is to create different image sizes or e.g. automatically convert DOCX document to PDF version.

There is set of predefined file handlers

### FileHandler

This handler does nothing with uploaded file. It always creates a `default` version with no transformations.
This File handler can be used with key `@handlers.file` or `@App\Services\Handlers\FileHandler`

**sample usage**

    parameters:
        configurations:
            default:
                handler: @handlers.file

### ImageHandler

This handler is used for image manipulations. It creates different versions specified in `versions` section.

Format of the version definition is `WIDTHxHEIGHT:RESIZE_FLAGS`. Resize flags are optional and they
are used from [Nette Image Resize](https://doc.nette.org/en/utils/images#toc-image-resize). Even the
flag piping is possible. If version definition is empty, no image transformations are done.

This File handler can be used with key `@handlers.image` or `@App\Services\Handlers\ImageHandler`

**sample usage**

    parameters:
        configurations:
            images:
                handler: @handlers.image
                versions:
                    original:
                    large: '1920x600:SHRINK_ONLY'
                    medium: '800x280:SHRINK_ONLY|STRETCH'
                    preview: '300x300:FILL'

<a name="usage"></a>
## Usage

Communication with the service uses REST API

### Create file

    POST http://localhost:8080/api/files

POST request requires data payload to be JSON, posting some info about file and data to be base64 encoded

    {
        "namespace": "images",
        "sign": "my-custom-identifier",
        "file": {
            "name": "test-image.jpg",
            "data": "data:image/jpeg;base64,/9j/4AAQSkZJRg...."
        }
    }

According to the given namespace, configuration section will be used. In this example with namespace 
"images" the configuration section "images" will be used with the handlers defined.

Sign parameter is optional. Value can be anything from simple value (string/number) to complex object or array. But this value will be forwarded to RabbitMQ event. Typical usage could be
to pass object identifier where is the file attached to. After that created event is triggered
and listener can assign the event to proper object.

### Get file

    GET http://localhost:8080/api/files/<id>

Where ID is unique file identifier. The response will look like

    {
        "id": "079c8e67-f1d4-4560-8584-128755a1cc20",
        "name": "test-image.jpg",
        "namespace": "test",
        "created_at": "2022-08-24T23:54:47+02:00",
        "updated_at": null,
        "versions": [
            {
                "id": "87595a77-3aae-4f30-b38f-5f3b8441c9f5",
                "file_id": "079c8e67-f1d4-4560-8584-128755a1cc20",
                "version": "medium",
                "size": 279056,
                "mime_type": "image/jpeg",
                "info": {
                    "width": 543,
                    "height": 360
                },
                "url": "http://localhost:8080/files/test/202208/079c8e67-f1d4-4560-8584-128755a1cc20/87595a77-3aae-4f30-b38f-5f3b8441c9f5"
            },
            ....
        ]
    }

### Delete image

    DELETE http://localhost:8080/api/files/<id>?sign=my-custom-identifier

Where ID is unique file identifier.

Sign parameter is optional. But this value will be forwarded to RabbitMQ event. Typical usage could be
to pass object identifier where is the file attached to. After that deleted event is triggered
and listener can assign the event to proper object.

## RabbitMQ

Storage service supports communication through RabbitMQ message broker. It allows the service
to be connected into microservices architecture and communicate with asynchronous patterns.
For this there are two message types

### Commands

Commands are intended to be used as asynchronous **requests** to service. Typical use case is 
to request file delete when another business object has been deleted. Command is **active**
communication request and supported types are:

#### Create new file 

Routing key: `storage.cmd.create`

> Notice that no events are triggered at this time. It means that sign
parameter is not used now.


    {
        "namespace": "images",
        "sign": "my-custom-identifier",
        "file": {
            "name": "test-image.jpg",
            "data": "data:image/jpeg;base64,/9j/4AAQSkZJRg...."
        }
    }


#### Delete file

Routing key: `storage.cmd.delete`

> Notice that no events are triggered at this time. It means that sign
parameter is not used now.

    {
        "id": "1a52a547-e0fa-403f-9be7-ee00ea3fc84e",
        "sign: "my-custom-identifier"
    }


### Events

Events are **passive** communication objects representing notification that something with service happened.
Supported events are:

#### File created 

Routing key: `storage.evt.created`

    {
        "id": "1a52a547-e0fa-403f-9be7-ee00ea3fc84e",
        "sign": "my-custom-identifier",
        "versions": {
            "preview": "image-path",
            "full": "image-path"
        }
    }

#### File deleted

Routing key: `storage.evt.deleted`

    {
        "id": "1a52a547-e0fa-403f-9be7-ee00ea3fc84e",
        "sign": "my-custom-identifier"
    }
