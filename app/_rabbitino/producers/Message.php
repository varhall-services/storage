<?php

namespace Varhall\Rabbitino\Producers;

use Nette\Utils\Json;
use Varhall\Utilino\ISerializable;

abstract class Message implements ISerializable
{
    protected array $headers = [];
    protected array $data = [];

    public abstract function key(): string;

    public function getHeaders(): array
    {
        return $this->headers;
    }

    public function toArray(): array
    {
        return $this->data;
    }

    public function toJson(): string
    {
        return Json::encode($this->toArray());
    }
}