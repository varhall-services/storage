<?php

namespace Varhall\Rabbitino\Producers;

use Contributte\RabbitMQ\Producer\Producer;

class Notifier
{
    protected Producer $producer;

    protected bool $enabled;

    public function __construct(Producer $producer, bool $enabled = true)
    {
        $this->producer = $producer;
        $this->enabled = $enabled;
    }

    public function getProducer(): Producer
    {
        return $this->producer;
    }

    public function isEnabled(): bool
    {
        return $this->enabled;
    }

    public function send(Message $message): void
    {
        if ($this->enabled) {
            $this->producer->publish($message->toJson(), $message->getHeaders(), $message->key());
        }
    }
}