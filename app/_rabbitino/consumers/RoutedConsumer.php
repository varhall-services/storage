<?php

namespace Varhall\Rabbitino\Consumers;

use Bunny\Message;
use Contributte\RabbitMQ\Consumer\IConsumer;
use Nette\Utils\Json;
use Tracy\Debugger;
use Tracy\ILogger;

class RoutedConsumer implements IConsumer
{
    protected array $receivers = [];

    public function register(string $pattern, Receiver $receiver)
    {
        $this->receivers[$pattern] = $receiver;
    }

    public function consume(Message $message): int
    {
        Debugger::log("Received message: {$message->content}, routing key: {$message->routingKey}", ILogger::DEBUG);

        foreach ($this->receivers as $pattern => $receiver) {
            $pattern = str_replace('*', '.+', $pattern);

            if (preg_match("/^{$pattern}$/i", $message->routingKey)) {
                try {
                    $data = Json::decode($message->content);

                    if (!$receiver->validate($data)) {
                        Debugger::log('Message is not valid', ILogger::WARNING);
                        return IConsumer::MESSAGE_REJECT;
                    }

                    $receiver->run($data);
                    return IConsumer::MESSAGE_ACK;

                } catch (\Exception $ex) {
                    Debugger::log($ex, ILogger::ERROR);
                    return IConsumer::MESSAGE_REJECT;
                }
            }
        }

        Debugger::log('No receiver found', ILogger::WARNING);
        return IConsumer::MESSAGE_REJECT;
    }
}