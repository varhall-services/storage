<?php

namespace Varhall\Rabbitino\Consumers;

use Nette\Schema\Expect;
use Nette\Schema\Processor;
use Nette\Schema\Schema;
use Nette\Schema\ValidationException;

abstract class Receiver
{
    public function validate($data): bool
    {
        try {
            $processor = new Processor();
            return !!$processor->process($this->schema(), $data);

        } catch (ValidationException $ex) {
            return false;
        }
    }

    protected function schema(): Schema
    {
        return Expect::mixed();
    }

    public function run($data): void
    {

    }
}