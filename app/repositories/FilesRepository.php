<?php

namespace App\Repositories;

use App\services\storages\IStorage;
use Varhall\Dbino\Events\DeleteArgs;
use Varhall\Dbino\Repository;


class FilesRepository extends Repository
{
    /** @var IStorage */
    protected $storage;

    public function __construct(IStorage $storage)
    {
        $this->storage = $storage;

        $this->on('deleting', function(DeleteArgs $args) {
            $this->storage->delete($args->instance);
        });
    }
}