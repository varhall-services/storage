<?php

namespace App\Rabbit\Events;

use Varhall\Rabbitino\Producers\Message;

class DeletedEvent extends Message
{
    public function __construct(string $id, mixed $sign = null)
    {
        $this->data = [ 'id' => $id, 'sign' => $sign ];
    }

    public function key(): string
    {
        return 'storage.evt.deleted';
    }
}