<?php

namespace App\Rabbit\Events;

use App\Models\File;
use Varhall\Rabbitino\Producers\Message;

class CreatedEvent extends Message
{
    public function __construct(File $file, mixed $sign = null)
    {
        $versions = $file->versions;
        $this->data = [
            'id'        => $file->id,
            'versions'  => array_combine(
                $versions->map(fn($x) => $x->version)->toArray(),
                $versions->map(fn($x) => $x->path)->toArray()
            ),
            'sign'      => $sign
        ];
    }

    public function key(): string
    {
        return 'storage.evt.created';
    }
}