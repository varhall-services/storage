<?php

namespace App\Rabbit\Receivers;

use App\Services\FilesService;
use Nette\Schema\Expect;
use Nette\Schema\Schema;
use Varhall\Rabbitino\Consumers\Receiver;
use Varhall\Utilino\Files\FileUtils;

class CreateReceiver extends Receiver
{
    protected FilesService $service;

    public function __construct(FilesService $service)
    {
        $this->service = $service;
    }

    protected function schema(): Schema
    {
        return Expect::structure([
            'namespace'     => Expect::string()->required(),
            'sign'          => Expect::mixed(),
            'custom_data'   => Expect::mixed(),
            'file'          => Expect::structure([
                'name'          => Expect::string()->required(),
                'data'          => Expect::string()->required()
            ])
        ]);
    }

    public function run($data): void
    {
        $file = FileUtils::fromBase64($data->file->data, $data->file->name);
        $this->service->save($file, $data->namespace, (array) ($data->custom_data ?? []));
    }
}