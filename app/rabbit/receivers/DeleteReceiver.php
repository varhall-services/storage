<?php

namespace App\Rabbit\Receivers;

use App\Models\File;
use Nette\InvalidArgumentException;
use Nette\Schema\Expect;
use Nette\Schema\Schema;
use Varhall\Rabbitino\Consumers\Receiver;

class DeleteReceiver extends Receiver
{
    protected function schema(): Schema
    {
        return Expect::structure([
            'id'    => Expect::string()->required(),
            'sign'  => Expect::mixed()
        ]);
    }

    public function run($data): void
    {
        $file = File::find($data->id);

        if (!$file) {
            throw new InvalidArgumentException('File not found');
        }

        $file->delete();
    }
}