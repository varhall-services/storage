<?php

namespace App\Presenters\ApiModule\Responses;

use App\Models\File;
use Varhall\Restino\Presenters\Results\Json;

class FullResponse extends Json
{
    public function __construct(File $file)
    {
        $data = array_merge($file->toArray(), [
            'versions'  => $file->versions->map(fn($version) => array_merge($version->toArray(), [ 'path' => $version->path ]))->toArray()
        ]);

        parent::__construct($data);
    }
}