<?php

namespace App\Presenters\ApiModule\Responses;

use App\Models\File;
use Varhall\Restino\Presenters\Results\Json;

class SimpleResponse extends Json
{
    public function __construct(File $file)
    {
        $versions = $file->versions;

        $data = [
            'id'        => $file->id,
            'versions'  => array_combine(
                $versions->map(fn($x) => $x->version)->toArray(),
                $versions->map(fn($x) => $x->path)->toArray()
            )
        ];

        parent::__construct($data);
    }
}