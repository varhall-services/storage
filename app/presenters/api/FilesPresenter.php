<?php

namespace App\Presenters\ApiModule;

use App\Models\File;
use App\Presenters\ApiModule\Responses\FullResponse;
use App\Presenters\ApiModule\Responses\SimpleResponse;
use App\Rabbit\Events\CreatedEvent;
use App\Rabbit\Events\DeletedEvent;
use App\Services\FilesService;
use Nette\Http\Response;
use Varhall\Rabbitino\Producers\Notifier;
use Varhall\Restino\Presenters\Results\Termination;

class FilesPresenter extends ApiPresenter
{
    protected FilesService $service;

    protected Notifier $notifier;

    public function __construct(FilesService $service, Notifier $notifier)
    {
        $this->service = $service;
        $this->notifier = $notifier;
    }

    protected function modelClass()
    {
        return File::class;
    }

    public function restGet($id, array $data = [])
    {
        $file = parent::restGet($id, $data);

        return filter_var($data['details'] ?? false, FILTER_VALIDATE_BOOLEAN) ? new FullResponse($file) : new SimpleResponse($file);
    }

    public function restCreate(array $data)
    {
        $upload = $this->getRequestFiles('file');

        if (count($upload) !== 1) {
            return new Termination('Single file must be uploaded', Response::S400_BadRequest);
        }

        $file = $this->service->save($upload[0], $data['namespace'], $data['custom_data'] ?? []);
        $this->notifier->send(new CreatedEvent($file, $data['sign'] ?? null));

        return new SimpleResponse($file);
    }

    public function restDelete($id)
    {
        $data = array_merge([ 'sign' => null ], $this->getParameter('data'));

        $result = parent::restDelete($id);
        $this->notifier->send(new DeletedEvent($id, $data['sign']));

        return $result;
    }

    protected function methodsOnly()
    {
        return ['get', 'create', 'delete'];
    }

    protected function validationDefinition()
    {
        return [
            'namespace'     => [ 'string:1..', 'required' ],
            'sign'          => [ 'mixed' ],
            'custom_data'   => [ 'array' ]
        ];
    }
}