<?php

namespace App\Presenters\ApiModule;

use Varhall\Restino\Presenters\Plugins\AllowedMethodPlugin;
use Varhall\Restino\Presenters\Plugins\CorsPlugin;
use Varhall\Restino\Presenters\Plugins\DateTimePlugin;
use Varhall\Restino\Presenters\Plugins\FilterPlugin;
use Varhall\Restino\Presenters\Plugins\TransformPlugin;
use Varhall\Restino\Presenters\Plugins\ValidatePlugin;
use Varhall\Securino\Presenters\Plugins\AuthenticationPlugin;
use Varhall\Restino\Presenters\RestPresenter;

/**
 * Base API presenter
 *
 * @author Ondrej Sibrava <sibrava@varhall.cz>
 */
abstract class ApiPresenter extends \Nette\Application\UI\Presenter
{
    use RestPresenter;

    public function startup()
    {
        parent::startup();

        $this->autoCanonicalize = FALSE;

        $this->plugin(AllowedMethodPlugin::class);
        $this->plugin(CorsPlugin::class);
        //$this->plugin(AuthenticationPlugin::class);
        $this->plugin(FilterPlugin::class)->only(['create', 'update', 'clone']);
        $this->plugin(TransformPlugin::class);
        $this->plugin(DateTimePlugin::class);
        $this->plugin(ValidatePlugin::class)->only(['create', 'update', 'clone']);

        $this->getHttpResponse()->setExpiration(null);
    }
}
