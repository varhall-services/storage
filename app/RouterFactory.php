<?php

declare(strict_types=1);

namespace App\Router;

use Nette\Application\Routers\Route;
use Nette\Application\Routers\RouteList;
use Nette\StaticClass;
use Varhall\Restino\Router\RestRoute;


final class RouterFactory
{
	use StaticClass;

	public static function createRouter(): RouteList
	{
		$router = new RouteList;

        $router[] = new RestRoute('api/<presenter>[/<id>]', [
            'module'    => 'Api',
            'action'    => 'restList',
            'id'        => null
        ]);

        //$router[] = new Route('files/<presenter>', [
        //    'module'    => 'Files',
        //]);


        return $router;
	}
}
