<?php

namespace App\Services\Storages;

use Nette\DI\Container;

class StorageFactory
{
    private Container $container;

    public function __construct(Container $container)
    {
        $this->container = $container;
    }

    public function create(string $type): IStorage
    {
        return $this->container->getService("storage.{$type}");
    }
}