<?php

namespace App\Services\Storages;

use App\Models\File;
use App\Models\Version;
use App\Services\Handlers\IFSObject;
use Nette\Http\FileUpload;
use Nette\Utils\Image;

interface IStorage
{
    public function save(Version $version, IFSObject $file): void;

    public function saveImage(Version $version, Image $image, ?int $type = null): void;

    public function saveFile(Version $version, FileUpload $file): void;

    public function delete(File $file): void;
}