<?php

namespace App\Services\Storages;

use App\Models\File;
use App\Models\Version;
use App\Services\Handlers\IFSObject;
use Nette\Http\FileUpload;
use Nette\Utils\Finder;
use Nette\Utils\Image;
use Varhall\Utilino\Utils\Path;

class LocalStorage implements IStorage
{
    protected string $root;

    public function __construct(string $root)
    {
        $this->root = $root;
    }

    public function save(Version $version, IFSObject $file): void
    {
        $directory = Path::combine($this->root, $version->file->directory());

        if (!file_exists($directory)) {
            mkdir($directory, 0777, true);
        }

        $file->save($this, $version);
    }

    public function saveImage(Version $version, Image $image, ?int $type = null): void
    {
        $image->save(Path::combine($this->root, $version->location()), null, $type);//exif_imagetype($upload->getTemporaryFile()));
    }

    public function saveFile(Version $version, FileUpload $file): void
    {
        $file->move(Path::combine($this->root, $version->location()));
    }

    public function delete(File $file): void
    {
        $path = Path::combine($this->root, $file->directory());

        if (!file_exists($path)) {
            return;
        }

        foreach (Finder::findFiles('*')->in($path) as $file) {
            unlink($file);
        }

        rmdir($path);
    }
}