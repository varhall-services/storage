<?php

namespace App\Services;

use App\Models\File;
use App\Models\Version;
use App\Services\Handlers\FileHandler;
use App\Services\Handlers\IHandler;
use App\Services\Storages\IStorage;
use Nette\DI\Container;
use Nette\Http\FileUpload;

class FilesService
{
    protected Container $container;

    protected IStorage $storage;

    protected array $configurations;


    public function __construct(Container $container, IStorage $storage, array $configurations)
    {
        $this->container = $container;
        $this->storage = $storage;
        $this->configurations = $configurations;
    }

    public function save(FileUpload $upload, string $namespace, array $data): File
    {
        $file = null;

        try {
            $configuration = $this->getConfiguration($namespace);
            $handler = is_string($configuration['handler']) ? $this->container->getByType($configuration['handler']) : $configuration['handler'];

            $file = File::create([
                'name'          => $upload->getUntrustedName(),
                'namespace'     => $namespace,
                'custom_data'   => $data
            ]);

            foreach ($configuration['versions'] ?? $this->defaultVersions() as $name => $cfg) {
                $this->createVersion($handler, $file, $upload, $name, $cfg);
            }

            return $file;

        } catch (\Exception $ex) {
            if ($file) {
                $file->delete();
            }

            throw $ex;
        }
    }

    protected function createVersion(IHandler $handler, File $file, FileUpload $upload, string $name, ?string $configuration): void
    {
        $fs = $handler->handle($upload, $configuration);

        $version = Version::create([
            'file_id'   => $file->id,
            'version'   => $name,
            'size'      => $upload->getSize(),
            'mime_type' => $upload->getContentType(),
            'info'      => $fs->info()
        ]);

        $this->storage->save($version, $fs);
    }

    protected function getConfiguration(string $namespace): array
    {
        if (array_key_exists($namespace, $this->configurations)) {
            return $this->configurations[$namespace];

        } else if (array_key_exists('default', $this->configurations)) {
            return $this->configurations['default'];
        }

        return [
            'handler'   => FileHandler::class,
            'versions'  => $this->defaultVersions()
        ];
    }

    protected function defaultVersions(): array
    {
        return [ 'original' => null ];
    }
}