<?php

namespace App\Services\Handlers;

use Nette\Http\FileUpload;

class FileHandler implements IHandler
{
    public function handle(FileUpload $upload, ?string $configuration): IFSObject
    {
        return new FSFile($upload);
    }
}