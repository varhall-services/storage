<?php

namespace App\Services\Handlers;

use App\Models\Version;
use App\Services\Storages\IStorage;
use Nette\Http\FileUpload;

class FSFile implements IFSObject
{
    protected FileUpload $file;

    public function __construct(FileUpload $file)
    {
        $this->file = $file;
    }

    public function info(): array
    {
        return [];
    }

    public function save(IStorage $storage, Version $version): void
    {
        $storage->saveFile($version, $this->file);
    }
}