<?php

namespace App\Services\Handlers;

use Nette\Http\FileUpload;

interface IHandler
{
    public function handle(FileUpload $upload, ?string $configuration): IFSObject;
}