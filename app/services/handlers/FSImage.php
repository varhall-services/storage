<?php

namespace App\Services\Handlers;

use App\Models\Version;
use App\Services\Storages\IStorage;
use Nette\Utils\Image;

class FSImage implements IFSObject
{
    protected Image $image;

    protected ?int $type;

    public function __construct(Image $image, ?int $type)
    {
        $this->image = $image;
        $this->type = $type;
    }

    public function info(): array
    {
        return [
            'width'     => $this->image->getWidth(),
            'height'    => $this->image->getHeight()
        ];
    }

    public function save(IStorage $storage, Version $version): void
    {
        $storage->saveImage($version, $this->image, $this->type);
    }
}