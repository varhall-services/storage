<?php

namespace App\Services\Handlers;

use App\Models\Version;
use App\Services\Storages\IStorage;

interface IFSObject
{
    public function info(): array;

    public function save(IStorage $storage, Version $version): void;
}