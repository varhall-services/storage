<?php

namespace App\Services\Handlers;

use Nette\Http\FileUpload;
use Nette\InvalidArgumentException;
use Nette\Utils\Image;

class ImageHandler implements IHandler
{
    public function handle(FileUpload $upload, ?string $configuration): IFSObject
    {
        if (!$upload->isImage()) {
            throw new InvalidArgumentException('Unsupported file type');
        }

        $image = $this->modifyImage($upload->toImage(), $configuration);

        return new FSImage($image, exif_imagetype($upload->getTemporaryFile()));
    }

    protected function modifyImage(Image $image, ?string $action): Image
    {
        if (!$action) {
            return $image;

        } else if (preg_match('/^([0-9]+)x([0-9]+)(:([A-Z_|]+))?$/', $action, $matches)) {
            $flags = Image::FIT;
            foreach (explode('|', $matches[4]) as $constant) {
                $flags = $flags | constant(Image::class . '::' . $constant);
            }

            $image = $image->resize($matches[1], $matches[2], $flags);

        } else if (is_callable($action)) {
            $image = call_user_func($action, $image);
        }

        return $image;
    }
}