<?php

declare(strict_types=1);

namespace App;

use Nette\Bootstrap\Configurator;

class Bootstrap
{
    public static function boot(): Configurator
    {
        $configurator = new Configurator;
        $appDir = dirname(__DIR__);

        $configurator->setDebugMode(getenv('APP_ENV') === 'development');
        $configurator->enableTracy($appDir . '/log');

        $configurator->setTimeZone('Europe/Prague');
        $configurator->setTempDirectory($appDir . '/temp');

        $configurator->createRobotLoader()
            ->addDirectory(__DIR__)
            ->register();

        $configurator->addConfig($appDir . '/config/common.neon');
        $configurator->addConfig($appDir . '/config/services.neon');
        $configurator->addConfig($appDir . '/config/local.neon');
        $configurator->addConfig($appDir . '/config/rabbitmq/rabbitmq.neon');

        $configurator->addDynamicParameters([
            'env' => array_merge([
                'STORAGE'           => 'local',

                'RABBIT_ENABLED'    => false,
                'RABBIT_HOST'       => 'rabbitmq',
                'RABBIT_USER'       => 'guest',
                'RABBIT_PASSWORD'   => 'guest',
                'RABBIT_PORT'       => 5672,
                //'RABBIT_QUEUE'      => 'storage',
                //'RABBIT_EXCHANGE'   => 'application',
            ], getenv()),
        ]);

        // fix %wwwDir% when running from console
        $configurator->addParameters(['wwwDir' => dirname(__DIR__) . '/www']);

        return $configurator;
    }

}
