<?php

namespace App\Models;

use App\repositories\FilesRepository;
use Nette\NotSupportedException;
use Varhall\Dbino\Model;
use Varhall\Dbino\Plugins\UuidPlugin;
use Varhall\Dbino\Traits\Timestamps;
use Varhall\Utilino\Collections\ICollection;
use Varhall\Utilino\Utils\Path;

class File extends Model
{
    use Timestamps;

    protected $casts = [
        'custom_data'   => 'json'
    ];

    /// RELATIONS

    public function versions(): ICollection
    {
        return $this->hasMany(Version::class, 'file_id');
    }


    /// DYNAMIC PROPERTIES

    public function directory(): string
    {
        if (!$this->isSaved()) {
            throw new NotSupportedException('Location is not available since the file was not saved');
        }

        //return Path::combine($this->namespace, $this->created_at->format('Ym'), $this->id);
        return Path::combine($this->namespace, $this->id);
    }

    /// CONFIGURATION

    protected function plugins()
    {
        return [
            new UuidPlugin('id')
        ];
    }

    protected function repository(): string
    {
        return FilesRepository::class;
    }

    protected function table()
    {
        return 'files';
    }
}