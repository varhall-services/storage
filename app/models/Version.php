<?php

namespace App\Models;

use Nette\InvalidStateException;
use Nette\NotSupportedException;
use Varhall\Dbino\Model;
use Varhall\Dbino\Plugins\UuidPlugin;
use Varhall\Utilino\Utils\Path;

class Version extends Model
{
    protected $casts = [
        'info'      => 'json'
    ];


    /// RELATIONS

    public function file(): File
    {
        return $this->belongsTo(File::class, 'file_id');
    }


    /// DYNAMIC PROPERTIES

    public function location(): string
    {
        return Path::combine($this->file()->directory(), $this->id);
    }

    public function path(): string
    {
        return implode('/', [ $this->file()->namespace, $this->file_id, $this->id ]);
    }


    /// CONFIGURATION

    protected function plugins()
    {
        return [
            new UuidPlugin('id')
        ];
    }

    protected function table()
    {
        return 'versions';
    }
}